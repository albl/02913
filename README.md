This is a complementary page for course 02913 - Advanced Analysis Techniques

[https://kurser.dtu.dk/course/02913](https://kurser.dtu.dk/course/02913)

Some general information about the course
* It is aimed for MS.c and Ph.D. students interested in modelling & analysis techniques rooted in formal methods (largely intended) from computer science.
* The course is also open to Ph.D. students from other Danish universities.
* Every year the course is instantiated on a different topic, closely related to the research going on at the section for [Software Systems Engineering](https://kurser.dtu.dk/course/02913).
* The course provides a research-based experience.

The format is usually as follows: 
* The course runs in the 3-week period in June (see [DTU's academic calendar](https://www.dtu.dk/english/education/student-guide/studying-at-dtu/academic-calendar)). 
* The first few days are lectures & talks that introduce the topic of the course. 
* The students then select one topic to develop in the rest of the course (studying a paper, short literature review on a specific topic, trying to solve a research problem, proof of concept tool, etc.). Sometimes, the students already have a topic in mind when they join the course.
* The teachers approve the student's choice and provide some feedback.
* The students write a conference-style short paper, submitted some towards the end of the last week (but on time go get some feedback before the final presentation).
* The last day is organized like a workshop/conference, with paper presentations.

Topics and student projects

2024
* Topic: Hot PhD Topics in Software Systems Engineering
* Student talks:
    * Automated Verification of Privacy
    * Observability in POMDPs
    * Safe and Secure Software-Defined Networks in P4
    * Formalizing and Verifying Deadlock Monitoring Algorithm
    * Fair Join Pattern Matching for Actors
    * Provably Secure Distributed Systems
    * Expected reward = weakest pre-expectation
    * Towards Immersive Process Science

2023
* Lecturers: Christoph Matheja, Andrey Rivkin
* Topic: Explaining and Monitoring System Behaviours
* Student projects/talks:
    * Probabilistic Runtime Monitoring  
    * Swarm Protocols for Coordination-Free Collaboration in Peer-to-Peer Systems 
    * Can-logic Automative Instruction Detection via Temporal Logic
    * A survey of security vulnerability identification methods using HyperLTL
    * On the Format of Verification Witnesses
    * Verification Witnesses for Termination

2022
* Lecturers: Alceste Scalas, Andrea Burattin, Sebastian Mödersheim
* Topic: Distributed systems, security and process mining
* Student projects/talks:
    * Encoding Petri Nets into CCS
    * Process Trees to Attack Trees
    * Applying Alpha Miner and Heuristic Minder Algorithms utilizing Event Data
    * Checking a Model Checker
    * Tools Used To Analyse Receipt-Freeness in FOO’92
    * Type Systems for Security Protocols in the Spi-Calculus
    * A Lite Pi Calculus typechecker in Haskell 

2021
* Lecturers: Alceste Scalas, Sebastian Mödersheim
* Topic: Process calculi for distributed systems and security
* Student projects/talks:
    * Automated Symbolic Verification of Stateful Protocols in Trusted Computing
    * An overview of security protocol composition
    * Process calculi in biology

2020
* Lecturers: Alberto Lluch Lafuente, Sebastian Mödersheim
* Topic: Model checking and security
* Student projects/talks:
    * The Tamarin prover
    * Reduction techniques for Boolean Networks
    * Smart contract verification
    * Alpha-Beta privacy
    * HyperLogics, with applications to security
    * Model checking c-CTL
    * Determining asymptotic running times of programs (with Isabelle)
    * Probabilistic model checking for Markov decision processes
